﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>
#include <set>
#include <list>

using namespace std;

list<int> ToBin(int a)
{
	list<int> bin;
	int temp = 0;

	while (a)
	{
		temp = a & 1;
		bin.push_front(temp);
		a >>= 1;
	}

	return bin;
}

int PowerUp(int a, int power, int m)
{
	list<int> bin = ToBin(power);
	if (bin.size() == 0)
	{
		return 1;
	}
	int z = a;

	auto iter = bin.begin();
	iter++;

	for (auto i = iter; i != bin.end(); i++)
	{
		z = z * z % m;
		if (*i == 1)
		{
			z = z * a % m;
		}
	}

	return z;
}

int EilerFunction(int n) {
	int result = n;

	for (int i = 2; i * i <= n; i++)
	{
		if (n % i == 0)
		{
			while (n % i == 0)
			{
				n /= i;
			}
			result -= result / i;
		}
	}

	if (n > 1)
	{
		result -= result / n;
	}

	return result;
}

list<int> FindDelitels(int n)
{
	list<int> dels;

	for (int i = 2; i < n / 2; i++)
	{
		if (n % i == 0)
		{
			dels.push_back(i);
			dels.push_back(n / i);
		}
	}

	dels.sort();
	dels.unique();

	return dels;
}

int FindGeneration(int n, list<int> dels)
{
	int i = 2;
	int count = 0;

	while (i < n)
	{
		count = 0;

		for (auto del : dels)
		{
			if (PowerUp(i, del, n) != 1)
			{
				count++;
			}
		}

		if (count == dels.size())
		{
			break;
		}

		i++;
	}

	return i;
}

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}

	return 0;
}

int FindPower(list<int> list, int a)
{
	int count = 0;
	for (int n : list)
	{
		if (n == a)
		{
			count++;
		}
	}

	return count;
}

vector<int> Factorization(int n, vector<int>& e)
{
	int delitel = 2;
	int q = 0, r = 0;
	int t = 0, k = 1;

	list<int> p;
	list<int> l;

	while (n > 1)
	{
		r = n % delitel;
		q = n / delitel;

		if (r == 0)
		{
			p.push_back(delitel);
			t++;
			n = q;
		}
		else
		{
			if (delitel < q)
			{
				delitel = NextPrimeNumber(delitel);
				k++;
			}
			else
			{
				p.push_back(n);
				t++;
				break;
			}
		}
	}

	int count = 0;

	for (auto iter = p.begin(); iter != p.end(); iter++)
	{
		l.push_back(*iter);
	}

	p.unique();

	vector<int> result = {begin(p), end(p)};
	for (int n : p)
	{
		count = FindPower(l, n);
		e.push_back(count);
	}

	return result;
}

int gcd(int a, int b, int& x, int& y) {
	if (a == 0)
	{
		x = 0; y = 1;
		return b;
	}

	int x1, y1;
	int d = gcd(b % a, a, x1, y1);

	x = y1 - (b / a) * x1;
	y = x1;

	return d;
}

bool IsExist(list<int> a, int b)
{
	for (auto elem : a)
	{
		if (elem == b)
		{
			return true;
		}
	}

	return false;
}

vector<int> PostrotnieE(list<int> fact, list<int> e, list<int> base)
{
	vector<int> result;

	auto iterFact = fact.begin();
	auto iterE = e.begin();

	for (auto iter = base.begin(); iter != base.end(); iter++)
	{
		if (iterFact != fact.end())
		{
			if (*iterFact == *iter)
			{
				result.push_back(*iterE);
				iterFact++;
				iterE++;
			}
			else
			{
				result.push_back(0);
			}
		}
		else
		{
			result.push_back(0);
		}
	}

	return result;
}

int NodBin(int a, int b) {
	int shift = 0;

	if (!a)
		return b;
	if (!b)
		return a;

	for (shift = 0; !((a | b) & 1); ++shift) {
		a >>= 1;
		b >>= 1;
	}

	while (!(a & 1))
	{
		a >>= 1;
	}

	do {
		while (!(b & 1))
		{
			b >>= 1;
		}

		if (a < b)
		{
			b -= a;
		}
		else
		{
			a -= (b = a - b);
		}

		b >>= 1;
	} while (b);

	return a << shift;
}

int KTO(vector<int> a, vector<int> p)
{
	int len = a.size();
	
	int M = 1;
	for (auto elem : p)
	{
		M *= elem;
	}

	vector<int> m;
	vector<int> mInverse;
	for (auto elem : p)
	{
		int temp = M / elem;

		int u, v;
		int d = gcd(temp % elem, elem, u, v);

		m.push_back(temp);
		mInverse.push_back(((u % elem) + elem) % elem);
	}

	int result = 0;
	for (int i = 0; i < len; i++)
	{
		result += m[i] * mInverse[i] * a[i] % M;
	}

	return result;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int p = 0;
	list<int> pDels;
	do
	{
		cout << "Введите простое число p: ";
		cin >> p;
		pDels = FindDelitels(p);
	} while (p < 0 || pDels.size() > 0 || p == 0);

	int eilerFunc = EilerFunction(p);
	int gFormed = FindGeneration(p, FindDelitels(eilerFunc));

	cout << gFormed << endl;
	
	int u, v;
	int d = gcd(gFormed, p, u, v);
	int gFormedInv = u;

	vector<int> g;
	int i = 0;

	for (i = 1; i < p; i++)
	{
		g.push_back(i);
	}

	int a = 0;
	do
	{
		cout << "Введите число a: ";
		cin >> a;
	} while (a >= p || a < 0);

	vector<int> eilerE;
	vector<int> eilerFactor = Factorization(eilerFunc, eilerE);

	vector<vector<int>> table;
	
	for (auto elem : eilerFactor)
	{
		int degree = eilerFunc / elem;
		int alpha = PowerUp(gFormed, degree, p);

		vector<int> r;
		for (int i = 0; i < elem; i++)
		{
			r.push_back(PowerUp(alpha, i, p));
		}

		table.push_back(r);
	}

	i = 0;
	vector<int> rights;

	for (auto elem : eilerFactor)
	{
		
		int y = 0;

		for (int j = 0; j < eilerE[i]; j++)
		{
			int degree = eilerFunc / (int)pow(elem, j + 1);
			int secondMult = PowerUp(gFormedInv, y, p);
			secondMult = ((secondMult % p) + p) % p;

			int b = PowerUp((a * secondMult) % p, degree, p);
			b = ((b % p) + p) % p;

			auto it = find(table[i].begin(), table[i].end(), b);
			int x = distance(table[i].begin(), it);

			y += x * (int)pow(elem, j);
		}

		rights.push_back(y);
		i++;
	}

	i = 0;
	vector<int> moduls;

	for (auto el : eilerFactor)
	{
		moduls.push_back((int)pow(el, eilerE[i]));
		i++;
	}

	int result = KTO(rights, moduls) % eilerFunc;
	cout << "Ответ: " << result << endl;

	return 0;
}
