﻿#include <iostream>
#include <list>
#include <set>

using namespace std;

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}
}

int FindPower(list<int> list, int a)
{
	int count = 0;
	for (int n : list)
	{
		if (n == a)
		{
			count++;
		}
	}

	return count;
}

int main()
{
	setlocale(LC_ALL, "Russian");

    int delitel = 2;
	int q = 0, r = 0;
	int t = 0, k = 1;
	bool flag = true;

	list<int> p;
	list<int> l;

	int n = 0;
	int b = 0;

	cout << "Введите число n: ";
	cin >> n;
	cout << "Введите границу b: ";
	cin >> b;

	while (n > 1)
	{
		r = n % delitel;
		q = n / delitel;

		if (r == 0)
		{
			p.push_back(delitel);
			t++;
			n = q;
		}
		else
		{
			if (delitel < q)
			{
				delitel = NextPrimeNumber(delitel);
				if (delitel > b)
				{
					flag = false;
					break;
				}
				k++;
			}
			else
			{
				p.push_back(n);
				t++;
				break;
			}
		}
	}

	if (flag)
	{
		cout << "Факторизация полная" << endl;
	}
	else
	{
		cout << "Факторизация неполная" << endl;
		cout << "Неразложенный остаток: " << n << endl;
	}

	set<int> mnoziteli;
	int count = 0;

	for (auto iter = p.begin(); iter != p.end(); iter++)
	{
		mnoziteli.insert(*iter);
	}

	for (int n : mnoziteli)
	{
		count = FindPower(p, n);
		l.push_back(count);

		cout << n << " " << count << endl;
	}
}


