﻿#include <list>
#include <iostream>
#include <set>
#include <ctime>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}

	return 0;
}

int FindPower(list<int> list, int a)
{
	int count = 0;
	for (int n : list)
	{
		if (n == a)
		{
			count++;
		}
	}

	return count;
}

list<int> NullStrings(int m1[20][20], int t, int k)
{
	int i = 0, j = 0;
	list<int> result;

	for (i = 0; i < t; i++)
	{
		j = 0;
		while (m1[i][j] == 0 && j <= k)
		{
			j++;
		}

		if (j == k)
		{
			result.push_back(i);
		}
	}

	return result;
}

list<int> StringToMass(string s)
{
	list<int> mas;

	for (auto sym : s)
	{
		mas.push_back(sym - '0');
	}

	return mas;
}

void outMas(const vector<int>& mas, int num, string s, list<list<int>>& mnoz)
{
	if (num == mas.size())
	{
		if (s.length() > 0)
		{
			mnoz.push_back(StringToMass(s));
		}
		return;
	}
	outMas(mas, num + 1, s, mnoz);

	s = s + to_string(mas[num]);

	outMas(mas, num + 1, s, mnoz);
}

list<int> SumVect(list<int> a, list<int> b)
{
	list<int> result;

	auto iter = b.begin();

	for (auto el : a)
	{
		result.push_back((el + *iter) % 2);
		iter++;
	}

	return result;
}

list<list<int>> FindAllStrings(vector<list<int>> mass)
{
	int len = mass.size();
	vector<int> arr;

	for (int i = 0; i < len; i++)
	{
		arr.push_back(i);
	}

	list<list<int>> mnoz;
	outMas(arr, 0, "", mnoz);

	int vectLen = mass[0].size();
	list<int> vect;
	for (int i = 0; i < vectLen; i++)
	{
		vect.push_back(0);
	}

	list<list<int>> result;

	for (auto podmnoz : mnoz)
	{
		vect.clear();
		for (int i = 0; i < vectLen; i++)
		{
			vect.push_back(0);
		}

		for (auto elem : podmnoz)
		{
			vect = SumVect(vect, mass[elem]);
		}

		result.push_back(vect);
	}

	return result;
}

list<list<int>> Gaus(int m1[20][20], int m2e[20][20], int ordRow[], int t, int k)
{
	int i = 0, j = 0, l = 0, c = 0;

	while (l < k)
	{
		j = l;

		while (j < t)
		{
			if (m1[ordRow[j]][l] == 0)
			{
				j++;
			}
			else
			{
				break;
			}
		}

		if (j == t)
		{
			l++;
			continue;
		}

		if (j > l)
		{
			c = ordRow[j];
			ordRow[j] = ordRow[l];
			ordRow[l] = c;
		}

		i = l + 1;

		while (i < t)
		{
			if (m1[ordRow[i]][l] == 1)
			{
				for (j = 0; j < k; j++)
				{
					m1[ordRow[i]][j] = (m1[ordRow[i]][j] + m1[ordRow[l]][j]) % 2;
				}

				for (j = 0; j < t; j++)
				{
					m2e[ordRow[i]][j] = (m2e[ordRow[i]][j] + m2e[ordRow[l]][j]) % 2;
				}
			}

			i++;
		}

		l++;
	}

	int m1New[20][20];
	int m2eNew[20][20];

	for (i = 0; i < t; i++)
	{
		for (j = 0; j < k; j++)
		{
			m1New[i][j] = m1[ordRow[i]][j];
		}
	}

	for (i = 0; i < t; i++)
	{
		for (j = 0; j < t; j++)
		{
			m2eNew[i][j] = m2e[ordRow[i]][j];
		}
	}

	list<int> nullString = NullStrings(m1New, t, k);

	list<int> reaultStrings;
	vector<list<int>> firstResult;

	for (int row : nullString)
	{
		reaultStrings.clear();

		for (i = 0; i < t; i++)
		{
			reaultStrings.push_back(m2eNew[row][i]);
		}

		firstResult.push_back(reaultStrings);
	}

	list<list<int>> result = FindAllStrings(firstResult);

	return result;
}

int NodBin(int a, int b) {
	int shift = 0;

	if (!a)
		return b;
	if (!b)
		return a;

	for (shift = 0; !((a | b) & 1); ++shift) {
		a >>= 1;
		b >>= 1;
	}

	while (!(a & 1))
	{
		a >>= 1;
	}

	do {
		while (!(b & 1))
		{
			b >>= 1;
		}

		if (a < b)
		{
			b -= a;
		}
		else
		{
			a -= (b = a - b);
		}

		b >>= 1;
	} while (b);

	return a << shift;
}

bool IsExist(list<int> a, int b)
{
	for (auto elem : a)
	{
		if (elem == b)
		{
			return true;
		}
	}

	return false;
}

list<int> FindDelitels(int n)
{
	list<int> dels;

	if (n < 0)
	{
		return dels;
	}

	for (int i = 2; i < n / 2; i++)
	{
		if (n % i == 0)
		{
			dels.push_back(i);
			dels.push_back(n / i);
		}
	}

	dels.sort();
	dels.unique();

	return dels;
}

bool IsDegree(int n)
{
	if (n < 0)
	{
		return true;
	}

	int x = 0, k = 2;
	int border = (int)(log(n) / log(2));
	bool result = false;

	for (k; k < border; k = NextPrimeNumber(k))
	{
		int sqr = pow(n, (float)(1 / (float)k));

		if (pow(sqr, k) == n)
		{
			result = true;
		}
	}

	return result;
}

list<int> factorization(int n)
{
	int delitel = 2;
	int q = 0, r = 0;
	int t = 0, k = 1;

	list<int> p;

	while (n > 1)
	{
		r = n % delitel;
		q = n / delitel;

		if (r == 0)
		{
			p.push_back(delitel);
			t++;
			n = q;
		}
		else
		{
			if (delitel < q)
			{
				delitel = NextPrimeNumber(delitel);
				k++;
			}
			else
			{
				p.push_back(n);
				t++;
				break;
			}
		}
	}

	return p;
}

int MultiplyElements(list<int> m)
{
	int mult = 1;
	for (auto a : m)
	{
		mult *= a;
	}

	return mult;
}

int Lezandr(int a, int p)
{
	a = a % p;
	int result = 0;
	list<int> mnoziteli = factorization(a);
	mnoziteli.push_front(1);

	list<int> symbols;

	if (a == 0)
	{
		return 0;
	}

	for (auto el : mnoziteli)
	{
		if (el == 1)
		{
			symbols.push_back(1);
		}
		else
		{
			if (el == -1)
			{
				if (((p - 1) / 2) % 2 == 0)
				{
					symbols.push_back(1);
				}
				else
				{
					symbols.push_back(-1);
				}
			}
			else
			{
				if (el == 2)
				{
					if (((int(pow(p, 2)) - 1) / 8) % 2 == 0)
					{
						symbols.push_back(1);
					}
					else
					{
						symbols.push_back(-1);
					}
				}
				else
				{
					list<int> pod_mnoziteli = factorization(el);

					if (pod_mnoziteli.size() > 1)
					{
						symbols.push_back(Lezandr(el, p));
					}
					else if (el != -1 && el != 1 && el != 2)
					{
						if ((((p - 1) / 2) % 2 == 0) || (((el - 1) / 2) % 2 == 0))
						{
							symbols.push_back(Lezandr(p, el));
						}
						else
						{
							symbols.push_back(-Lezandr(p, el));
						}
					}
				}
			}
		}

		result = MultiplyElements(symbols);
	}

	return result;
}

bool IsConsist(int n, list<int> base, list<int>& e)
{
	if (n == 0)
	{
		return false;
	}
	
	int count = 0;
	base.pop_front();

	for (auto el : base)
	{
		count = 0;

		while (n % el == 0)
		{
			n /= el;
			count++;
		}

		e.push_back(count);
	}

	bool result;
	n == 1 ? result = true : result = false;

	return result;
}

int main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");

	list<int> base = {-1, 2};

	int n = 0;
	cout << "Введите число n ";
	cin >> n;

	int sqrN = (int)sqrt(n);
	list<int> dels = FindDelitels(n);

	while (IsDegree(n) || n < 0 || dels.size() == 0 || n % 2 == 0)
	{
		cout << "Вы ввели неправильное число введите заново: ";
		cin >> n;
		dels = FindDelitels(n);
		sqrN = (int)sqrt(n);
	}

	int l = 3;
	int border = 0;
	cout << "Введите границу базы: ";
	cin >> border;

	while (l < border)
	{
		if (Lezandr(n, l) == 1)
		{
			base.push_back(l);
		}
		l = NextPrimeNumber(l);
	}

	cout << "База: ";
	for (auto el : base)
	{
		cout << el << " ";
	}
	cout << endl;

	int t = base.size();
	int k = base.size();

	list<int> a;
	list<int> b;
	list<list<int>> matrixE;

	list<int> e;
	int tempA = 0;
	int tempB = 0;

	int i = 0, j = 0;
	int count = 0;
	int degree = 1;
	int z = 0;
	int u = 0, v = 0;

	while(true)
	{
		t++;

		while (count < t && z < n)
		{
			e.clear();
			bool flag;

			tempA = sqrN + (z * (int)pow(-1, degree));
			tempB = ((int)pow(tempA, 2) - n);

			if (tempB == -1)
			{
				for (auto el : base)
				{
					e.push_front(0);
				}
				*(e.begin())++;

				flag = true;
			}
			else if (tempB == 1)
			{
				for (auto el : base)
				{
					e.push_front(0);
				}
				*(e.begin()) = 2;

				flag = true;
			}
			else 
			{
				flag = IsConsist(abs(tempB), base, e);
				tempB < 0 ? e.push_front(1) : e.push_front(0);
			}

			if (flag && !IsExist(a, tempA))
			{
				a.push_back(tempA);
				b.push_back(tempB);
				matrixE.push_back(e);
				count++;
			}

			if (degree % 2 == 1)
			{
				z++;
			}
			degree++;
		}

		if (count < t)
		{
			cout << "Не удается найти решение для данного примера" << endl;
			return 0;
		}

		int m2e[20][20];
		int m1[20][20];
		int ordRow[20];
		int copyMatrixE[20][20];

		i = 0;
		for (auto n : matrixE)
		{
			j = 0;

			for (int a : n)
			{
				copyMatrixE[i][j] = a;
				m1[i][j] = a % 2;
				j++;
			}
			ordRow[i] = i;
			i++;
		}

		cout << "MATRIX " << endl;

		for (i = 0; i < t; i++)
		{
			for (j = 0; j < k; j++)
			{
				cout << m1[i][j] << " ";
			}
			cout << endl;
		}

		cout << "------------------------------------------------------------" << endl;

		for (i = 0; i < t; i++)
		{
			for (j = 0; j < t; j++)
			{
				if (i == j)
				{
					m2e[i][j] = 1;
				}
				else
				{
					m2e[i][j] = 0;
				}
			}
		}

		list<list<int>> results = Gaus(m1, m2e, ordRow, t, k);

		int x = 1, y = 1;
		i = 0;
		j = 0;

		for (auto vector : results)
		{
			auto iter = a.begin();
			x = 1, y = 1;
			u = 0, v = 0;

			for (int el : vector)
			{
				if (el == 1)
				{
					x *= *iter;
					x = x % n;
				}
				iter++;
			}

			i = 0;
			j = 0;
			int sum = 0;
			for (auto el1 : base)
			{
				i = 0;
				sum = 0;

				for (int el : vector)
				{
					if (el == 1)
					{
						sum += copyMatrixE[i][j];
					}

					i++;
				}

				sum = sum / 2;
				y *= pow(el1, sum);
				j++;
			}

			x = x % n;
			y = y % n;

			cout << "X " << x << " Y " << y << endl;

			if (y != x)
			{
				u = NodBin(abs(x + y), n);
				v = NodBin(abs(x - y), n);
				cout << "U " << u << " V " << v << endl;
				if (u == 1 || v == 1 || (u * v != n))
				{
					cout << "Пара " << x << " и " << y << " не подходит" << endl;
					cout << "Произведение u на v = " << u * v << endl;
					cout << endl;
				}
				else
				{
					break;
				}
			}
		}

		if (u != 0 && v != 0 && u != n && v != n && u * v == n)
		{
			break;
		}
	}

	cout << "Ответ " << u << "  " << v << endl;

	return 0;
}