﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>
#include <set>
#include <list>

using namespace std;

list<int> ToBin(int a)
{
	list<int> bin;
	int temp = 0;

	while (a)
	{
		temp = a & 1;
		bin.push_front(temp);
		a >>= 1;
	}

	return bin;
}

int PowerUp(int a, int power, int m)
{
	list<int> bin = ToBin(power);
	if (bin.size() == 0)
	{
		return 1;
	}
	int z = a;

	auto iter = bin.begin();
	iter++;

	for (auto i = iter; i != bin.end(); i++)
	{
		z = z * z % m;
		if (*i == 1)
		{
			z = z * a % m;
		}
	}

	return z;
}

int EilerFunction(int n) {
	int result = n;

	for (int i = 2; i * i <= n; i++)
	{
		if (n % i == 0)
		{
			while (n % i == 0)
			{
				n /= i;
			}
			result -= result / i;
		}
	}

	if (n > 1)
	{
		result -= result / n;
	}

	return result;
}

list<int> FindDelitels(int n)
{
	list<int> dels;

	for (int i = 2; i < n / 2; i++)
	{
		if (n % i == 0)
		{
			dels.push_back(i);
			dels.push_back(n / i);
		}
	}

	dels.sort();
	dels.unique();

	return dels;
}

int FindGeneration(int n, list<int> dels)
{
	int i = 2;
	int count = 0;

	while (i < n)
	{
		count = 0;

		for (auto del : dels)
		{
			if (PowerUp(i, del, n) != 1)
			{
				count++;
			}
		}

		if (count == dels.size())
		{
			break;
		}

		i++;
	}

	return i;
}

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}

	return 0;
}

int FindPower(list<int> list, int a)
{
	int count = 0;
	for (int n : list)
	{
		if (n == a)
		{
			count++;
		}
	}

	return count;
}

list<int> IsSmoothB(int n, int b, bool& flag, list<int>& e)
{
	if (n == 1)
	{
		flag = false;
	}

	int delitel = 2;
	int q = 0, r = 0;
	int t = 0, k = 1;

	list<int> p;
	list<int> l;

	while (n > 1)
	{
		r = n % delitel;
		q = n / delitel;

		if (r == 0)
		{
			if (delitel > b)
			{
				flag = false;
				break;
			}
			else
			{
				p.push_back(delitel);
				t++;
				n = q;
			}
		}
		else
		{
			if (delitel < q)
			{
				if (delitel > b)
				{
					flag = false;
					break;
				}
				delitel = NextPrimeNumber(delitel);
				k++;
			}
			else
			{
				if (n > b)
				{
					flag = false;
					break;
				}
				p.push_back(n);
				t++;
				break;
			}
		}
	}

	int count = 0;

	for (auto iter = p.begin(); iter != p.end(); iter++)
	{
		l.push_back(*iter);
	}

	p.unique();

	for (int n : p)
	{
		count = FindPower(l, n);
		e.push_back(count);
	}

	return p;
}

int gcd(int a, int b, int& x, int& y) {
	if (a == 0)
	{
		x = 0; y = 1;
		return b;
	}

	int x1, y1;
	int d = gcd(b % a, a, x1, y1);

	x = y1 - (b / a) * x1;
	y = x1;

	return d;
}

void Composition(vector<int>& i, vector<int>& j, int k)
{
	for (int d = 0; d < i.size(); d++)
	{
		i[d] = i[d] + j[d] * k;
	}
}

void DivRow(vector<int>& d, int k)
{
	if (k == 0)
	{
		return;
	}

	for (int i = 0; i < d.size(); i++)
	{
		d[i] = d[i] / k;
	}
}

bool Solve(vector<vector<int>>& a)
{
	for (int k = 0; k < a.size(); k++)
	{
		if (a[k][k] == 0)
		{
			for (int i = k + 1; i < a.size(); i++)
			{
				if (a[i][k] > 0)
				{
					swap(a[i], a[k]);
					break;
				}
			}
		}
		
		if (a[k][k] == 0)
		{
			return false;
		}
		else
		{
			DivRow(a[k], a[k][k]);
		}

		for (int i = k + 1; i < a.size(); i++)
		{
			Composition(a[i], a[k], -a[i][k]);
		}
	}

	for (int k = a.size() - 1; k >= 0; k--)
	{
		for (int i = k - 1; i >= 0; i--)
		{
			Composition(a[i], a[k], -a[i][k]);
		}
	}

	return true;
}

bool IsExist(vector<int> a, int b)
{
	for (auto elem : a)
	{
		if (elem == b)
		{
			return true;
		}
	}

	return false;
}

vector<int> PostrotnieE(list<int> fact, list<int> e, vector<int> base)
{
	vector<int> result;

	auto iterFact = fact.begin();
	auto iterE = e.begin();

	for (auto iter = base.begin(); iter != base.end(); iter++)
	{
		if (iterFact != fact.end())
		{
			if (*iterFact == *iter)
			{
				result.push_back(*iterE);
				iterFact++;
				iterE++;
			}
			else
			{
				result.push_back(0);
			}
		}
		else
		{
			result.push_back(0);
		}
	}

	return result;
}

int NodBin(int a, int b) {
	int shift = 0;

	if (!a)
		return b;
	if (!b)
		return a;

	for (shift = 0; !((a | b) & 1); ++shift) {
		a >>= 1;
		b >>= 1;
	}

	while (!(a & 1))
	{
		a >>= 1;
	}

	do {
		while (!(b & 1))
		{
			b >>= 1;
		}

		if (a < b)
		{
			b -= a;
		}
		else
		{
			a -= (b = a - b);
		}

		b >>= 1;
	} while (b);

	return a << shift;
}

bool IsCorrect(vector<vector<int>> matrix, int n)
{
	int count = 0;
	
	for (int i = 0; i < n; i++)
	{
		count = 0;

		for (int j = 0; j < n; j++)
		{
			if (matrix[j][i] == 0)
			{
				count++;
			}
		}

		if (count == n)
		{
			return false;
		}
	}

	return true;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int p = 0;
	list<int> pDels;
	do
	{
		cout << "Введите простое число p: ";
		cin >> p;
		pDels = FindDelitels(p);
	} while (p < 0 || pDels.size() > 0 || p == 0);

	int eilerFunc = EilerFunction(p);
	int gFormed = FindGeneration(p, FindDelitels(eilerFunc));

	cout << gFormed << endl;
	vector<int> g;
	int i = 0;

	for (i = 1; i < p; i++)
	{
		g.push_back(i);
	}

	int a = 0;
	do
	{
		cout << "Введите число a: ";
		cin >> a;
	} while (a >= p || a < 0);

	vector<int> base;
	int temp = -1;

	while (temp != 0)
	{
		cout << "Введите число базы ";
		cin >> temp;

		if (IsExist(base, temp))
		{
			cout << "Такое число уже есть в базе" << endl;
		}

		if (temp != 0 && !IsExist(base, temp))
		{
			base.push_back(temp);
		}
	}
	int t = base.size();

	vector<int> temps;
	vector<int> b;
	vector<vector<int>> matrixE;

	list<int> e;
	list<int> fact;
	temp = 0;
	int tempB = 0;
	
	int count = 0;
	bool flag = true;
	bool isSolving = false;

	do 
	{
		temps.clear();
		b.clear();
		matrixE.clear();

		e.clear();
		fact.clear();
		temp = 0;
		tempB = 0;

		count = 0;
		flag = true;

		do
		{
			count = 0;
			temps.clear();
			b.clear();
			matrixE.clear();
			fact.clear();
			e.clear();

			while (count < t)
			{
				flag = true;
				e.clear();

				temp = 2 + rand() % (p - 2);
				tempB = PowerUp(gFormed, temp, p);

				auto it = --base.end();
				fact = IsSmoothB(tempB, *it, flag, e);

				for (auto elem : e)
				{
					if (NodBin(elem, eilerFunc) != 1)
					{
						flag = false;
						break;
					}
				}

				if (flag && !IsExist(temps, temp))
				{
					temps.push_back(temp);
					b.push_back(tempB);
					matrixE.push_back(PostrotnieE(fact, e, base));
					count++;
				}
			}
		} while (!IsCorrect(matrixE, t));

		i = 0;
		for (int j = 0; j < matrixE.size(); j++)
		{
			matrixE[j].push_back(temps[i]);
			i++;
		}
		
		isSolving = Solve(matrixE);
	} while (!isSolving);

	vector<int> answers;
	for (auto row : matrixE)
	{
		int len = row.size();
		answers.push_back(row[len - 1]);
	}

	vector<int> s;
	
	while (true)
	{
		flag = true;
		e.clear();

		temp = 1 + rand() % (p - 1);
		tempB = PowerUp(gFormed, temp, p);
		tempB = a * tempB % p;

		auto it = --base.end();
		fact = IsSmoothB(tempB, *it, flag, e);

		if (flag)
		{
			s = PostrotnieE(fact, e, base);
			break;
		}
	}

	int result = 0;

	for (int i = 0; i < t; i++)
	{
		result += s[i] * answers[i] % eilerFunc;
	}
	result = ((result - temp) % eilerFunc + eilerFunc) % eilerFunc;

	cout << "Ответ: " << result << endl;

	return 0;
}
