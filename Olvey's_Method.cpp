﻿#include <iostream>
#include <cmath>

using namespace std;

double qbrt(double x)
{
    return pow(x, 1.0 / 3.0);
}

int main()
{
    setlocale(LC_ALL, "Russian");

    int n = 0, d = 0;
    int r1 = 0, r2 = 0;
    int q = 0, s = 0;
    int r = 0;
    bool flag = true;
    
    cout << "Введите число: ";
    cin >> n;

    d = 2 * round(qbrt(n)) + 1;

    r1 = n % d;
    r2 = n % (d - 2);
    q = 4 * ( round(n / (d - 2) ) - round(n / d) );
    s = sqrt(n);

    do
    {
        d += 2;
        if (d > s)
        {
            flag = false;
            break;
        }

        r = 2 * r1 - r2 + q;

        if (r < 0)
        {
            r += d;
            q += 4;
        }

        while (r >= d)
        {
            r -= d;
            q -= 4;
        }

        r2 = r1;
        r1 = r;
    } 
    while (r != 0);

    if (!flag)
    {
        cout << "Делителя в данном диапазоне нет" << endl;
    }
    else
    {
        cout << "Делитель числа " << n << " : " << d << endl;
    }
     
}