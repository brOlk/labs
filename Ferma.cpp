﻿#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	
	int n = 0;
	int a = 0, b = 0;
	int x = 0, z = 0;
	int y = 0;
	
	do
	{
		cout << "Введите n: ";
		cin >> n;
	} while (n % 2 == 0);

	x = int(sqrt(n));

	cout << x  << endl;

	if (pow(x, 2) == n)
	{
		a = b = x;
		cout << "Делители n: " << a << " " << b << endl;
		return 0;
	}

	int check = (n + 9) / 6;
	bool prime = false;

	do 
	{
		x++;
		cout << x << "    " << pow(x, 2) << "    ";
		if (x > check)
		{
			cout << "n - простое" << endl;
			prime = true;
			break;
		}

		z = pow(x, 2) - n;
		y = round(sqrt(z));
		cout << z << "    " << sqrt(z) << endl;
	} while (pow(y, 2) != z);

	if (!prime)
	{
		a = x + y;
		b = x - y;
		cout << "Делители n: " << a << " " << b << endl;
	}
}


