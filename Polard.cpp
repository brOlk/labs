﻿#include <iostream> 
#include <math.h>

using namespace std;

int f(int n, int x)
{
    int f = 0;
    f = (x * x + 1) % n;
    return f;
}

int Nod(int a, int b)
{
    int r = 0;
    while (a % b != 0)
    {
        r = a % b;
        a = b;
        b = r;
    }
    return b;
}

int NodBin(int a, int b) {
    int shift = 0;
    
    if (!a)
        return b;
    if (!b)
        return a;

    for (shift = 0; !((a | b) & 1); ++shift) {
        a >>= 1;
        b >>= 1;
    }

    while (!(a & 1))
    {
        a >>= 1;
    }

    do {
        while (!(b & 1))
        {
            b >>= 1;
        }
            
        if (a < b)
        {
            b -= a;
        }
        else
        {
            a -= (b = a - b);
        }

        b >>= 1;
    } while (b);

    return a << shift;
}

int Valid(int n)
{
    while (n % 2 == 0)
    {
        n >>= 1;
    }

    return n;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    int n = 0;
    int a = 2, b = 2, d = 1;

    cout << "Введите n (нечетное, составное): ";
    cin >> n;

    while (n < 0)
    {
        cout << "Вы ввели отрицательное число введите заново: ";
        cin >> n;
    }

    n = Valid(n);
    cout << "Исправленное число: " << n << endl;

    while (d == 1)
    {
        a = f(n, a);
        b = f(n, f(n, b));

        if (a == b)
        {
            cout << "Отказ" << endl;
            return 0;
        }

        d = NodBin(abs(a - b), n);
    }

    cout << "Ответ: " << d << endl;
    return 0;
}