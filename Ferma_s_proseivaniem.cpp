﻿#include <iostream>
#include <cmath>
#include <list>
#include <set>

using namespace std;

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}
}

list<int> factorization(int n)
{
	int delitel = 2;
	int q = 0, r = 0;
	int t = 0, k = 1;

	list<int> p;

	while (n > 1)
	{
		r = n % delitel;
		q = n / delitel;

		if (r == 0)
		{
			p.push_back(delitel);
			t++;
			n = q;
		}
		else
		{
			if (delitel < q)
			{
				delitel = NextPrimeNumber(delitel);
				k++;
			}
			else
			{
				p.push_back(n);
				t++;
				break;
			}
		}
	}

	return p;
}

int MultiplyElements(list<int> m)
{
	int mult = 1;
	for (auto a : m)
	{
		mult *= a;
	}

	return mult;
}

int Lezandr(int a, int p)
{
	a = a % p;
	int result = 0;
	list<int> mnoziteli = factorization(a);
	mnoziteli.push_front(1);
	
	list<int> symbols;

	if (a == 0)
	{
		return 1;
	}

	for (auto el : mnoziteli)
	{
		if (el == 1)
		{
			symbols.push_back(1);
		}
		else
		{
			if (el == -1)
			{
				if (((p - 1) / 2) % 2 == 0)
				{
					symbols.push_back(1);
				}
				else
				{
					symbols.push_back(-1);
				}
			}
			else
			{
				if (el == 2)
				{
					if (((int(pow(p, 2)) - 1) / 8) % 2 == 0)
					{
						symbols.push_back(1);
					}
					else
					{
						symbols.push_back(-1);
					}
				}
				else
				{
					list<int> pod_mnoziteli = factorization(el);

					if (pod_mnoziteli.size() > 1)
					{
						symbols.push_back(Lezandr(el, p));
					}
					else if (el != -1 && el != 1 && el != 2)
					{
						if ((((p - 1) / 2) % 2 == 0) || (((el - 1) / 2) % 2 == 0))
						{
							symbols.push_back(Lezandr(p, el));
						}
						else
						{
							symbols.push_back(-Lezandr(p, el));
						}
					}
				}
			}
		}
		result = MultiplyElements(symbols);
	}

	return result;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int n = 0;
	int a = 0, b = 0;
	int x = 0, z = 0;
	int y = -1;

	do
	{
		cout << "Введите n: ";
		cin >> n;
	} while (n % 2 == 0);

	x = int(sqrt(n));

	cout << x << endl;

	if (pow(x, 2) == n)
	{
		a = b = x;
		cout << "Делители n: " << a << " " << b << endl;
		return 0;
	}

	int m[50];
	int i = 0;

	do 
	{
		cout << "Введите " << i << " модуль: ";
		cin >> m[i];
		i++;
	} while (m[i - 1] != 0);

	int len = i - 1;
	int maxj = m[len - 1];
	int s[50][50];
	int nmod = 0;
	int prom = 0;

	for (int i = 0; i < len; i++)
	{
		nmod = n % m[i];
		cout << nmod << endl;

		for (int j = 0; j < maxj; j++)
		{
			if (j < m[i])
			{
				prom = pow(j, 2) - nmod;
				if (prom < 0)
				{
					prom += m[i];
				}
				
				if (Lezandr(prom, m[i]) == 1 || (prom == 0))
				{
					s[i][j] = 1;
				}
				else
				{
					s[i][j] = 0;
				}
			}
			else
			{
				s[i][j] = -1;
			}
		}
	}

	for (int i = 0; i < len; i++)
	{
		for (int j = 0; j < maxj; j++)
		{
			cout << s[i][j] << "\t";
		}
		cout << endl;
	}

	cout << x << "   ";
	int r[50];
	for (int i = 0; i < len; i++)
	{
		r[i] = x % m[i];
		cout << r[i] << "  ";
	}
	cout << endl;

	int check = (n + 9) / 6;
	bool flag = true;
	bool prime = false;

	do
	{
		flag = true;

		cout << "  " << "   ";
		for (int i = 0; i < len; i++)
		{
			flag &= (s[i][r[i]] == 1);
			cout << s[i][r[i]] << "  ";
		}

		if (flag)
		{
			z = pow(x, 2) - n;
			y = round(sqrt(z));
			if (int(pow(y, 2)) == z) 
			{
				break;
			}
			cout << x << "  " << n << "    " << z << endl;
		}
		cout << endl;

		x++;
		if (x > check)
		{
			cout << "n - простое" << endl;
			prime = true;
			break;
		}


		cout << x << "   ";
		for (int i = 0; i < len; i++)
		{
			r[i] = (r[i] + 1) % m[i];
			cout << r[i] << "  ";
		}
		cout << endl;
	} while (true);

	if (!prime) {
		a = x + y;
		b = x - y;
		cout << "Делители n: " << a << " " << b << endl;
	}
}


