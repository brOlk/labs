﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>
#include <set>
#include <list>

using namespace std;

int gcd(int a, int b, int& x, int& y) {
	if (a == 0)
	{
		x = 0; y = 1;
		return b;
	}

	int x1, y1;
	int d = gcd(b % a, a, x1, y1);

	x = y1 - (b / a) * x1;
	y = x1;

	return d;
}

list<int> ToBin(int a)
{
	list<int> bin;
	int temp = 0;

	while (a)
	{
		temp = a & 1;
		bin.push_front(temp);
		a >>= 1;
	}

	return bin;
}

int PowerUp(int a, int power, int m)
{
	list<int> bin = ToBin(power);
	if (bin.size() == 0)
	{
		return 1;
	}

	int z = a;

	auto iter = bin.begin();
	iter++;

	for (auto i = iter; i != bin.end(); i++)
	{
		z = z * z % m;
		if (*i == 1)
		{
			z = z * a % m;
		}
	}

	return z;
}

int EilerFunction(int n) {
	int result = n;

	for (int i = 2; i * i <= n; i++)
	{
		if (n % i == 0) 
		{
			while (n % i == 0)
			{
				n /= i;
			}
			result -= result / i;
		}
	}
		
	if (n > 1)
	{
		result -= result / n;
	}

	return result;
}

list<int> FindDelitels(int n)
{
	list<int> dels;

	if (n == 0)
	{
		dels.push_back(0);
		return dels;
	}

	for (int i = 2; i < n / 2; i++)
	{
		if (n % i == 0)
		{
			dels.push_back(i);
			dels.push_back(n / i);
		}
	}

	dels.sort();
	dels.unique();

	return dels;
}

int FindGeneration(int n, list<int> dels)
{
	int i = 2;
	int count = 0;
	
	while (i < n)
	{
		count = 0;

		for (auto del : dels)
		{
			if (PowerUp(i, del, n) != 1)
			{
				count++;
			}
		}

		if (count == dels.size())
		{
			break;
		}

		i++;
	}

	return i;
}

int IsConsist(int x, vector<int> s1, vector<int> s2, vector<int> s3)
{
	if (find(s1.begin(), s1.end(), x) != s1.end())
	{
		return 1;
	}
	else if (find(s2.begin(), s2.end(), x) != s2.end())
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

vector<int> FunctionF(vector<int> xyz, int a, int eilerFunc, int gFormed, int g, vector<int> s1, vector<int> s2, vector<int> s3)
{
	switch (IsConsist(xyz[0], s1, s2, s3))
	{
		case 1:
			xyz[0] = a * xyz[0] % g;
			xyz[1] = (xyz[1] + 1) % eilerFunc;
			break;
		case 2:
			xyz[0] = (int)pow(xyz[0], 2) % g;
			xyz[1] = 2 * xyz[1] % eilerFunc;
			xyz[2] = 2 * xyz[2] % eilerFunc;
			break;
		case 3:
			xyz[0] = gFormed * xyz[0] % g;
			xyz[2] = (xyz[2] + 1) % eilerFunc;
			break;
	}

	return xyz;
}

int NodBin(int a, int b) {
	int shift = 0;

	if (!a)
		return b;
	if (!b)
		return a;

	for (shift = 0; !((a | b) & 1); ++shift) {
		a >>= 1;
		b >>= 1;
	}

	while (!(a & 1))
	{
		a >>= 1;
	}

	do {
		while (!(b & 1))
		{
			b >>= 1;
		}

		if (a < b)
		{
			b -= a;
		}
		else
		{
			a -= (b = a - b);
		}

		b >>= 1;
	} while (b);

	return a << shift;
}

int NodTrex(int a, int b, int c)
{
	return NodBin(NodBin(a, b), c);
}

int main()
{
	setlocale(LC_ALL, "Russian");
	
	int p = 0;
	list<int> pDels;

	do
	{
		cout << "Введите простое число p: ";
		cin >> p;
		pDels = FindDelitels(p);
	} while (pDels.size() != 0 && p == 0);


	int eilerFunc = EilerFunction(p);
	int gFormed = FindGeneration(p, FindDelitels(eilerFunc));
	cout << gFormed << endl;
	vector<int> g;
	int i = 0;

	for (i = 1; i < p; i++)
	{
		g.push_back(i);
	}

	int a = 0;
	do
	{
		cout << "Введите число a: ";
		cin >> a;
	} while (a >= p);

	vector<int> s1, s2, s3;

	for (auto el : g)
	{
		switch (el % 3)
		{
			case 1:
				s1.push_back(el);
				continue;
			case 2: 
				s2.push_back(el);
				continue;
			case 0:
				s3.push_back(el);
				continue;
		}
	}

	vector<int> xyz1 = {1, 0, 0}, xyz2 = { 1, 0, 0 };

	xyz1 = FunctionF(xyz1, a, eilerFunc, gFormed, p, s1, s2, s3);
	xyz2 = FunctionF(FunctionF(xyz2, a, eilerFunc, gFormed, p, s1, s2, s3), a, eilerFunc, gFormed, p, s1, s2, s3);

	while (xyz1[0] != xyz2[0])
	{
		
		xyz1 = FunctionF(xyz1, a, eilerFunc, gFormed, p, s1, s2, s3);
		xyz2 = FunctionF(FunctionF(xyz2, a, eilerFunc, gFormed, p, s1, s2, s3), a, eilerFunc, gFormed, p, s1, s2, s3);
	}

	if (xyz1[1] == xyz2[1])
	{
		cout << "Фатальный отказ" << endl;
		return 0;
	}

	int u, v;

	int leftMn = ((xyz1[1] - xyz2[1]) % eilerFunc + eilerFunc) % eilerFunc;
	int rightMn = ((xyz2[2] - xyz1[2]) % eilerFunc + eilerFunc) % eilerFunc;
	int nod3 = NodTrex(leftMn, rightMn, eilerFunc);
	
	if (nod3 != 1)
	{
		leftMn /= nod3;
		rightMn /= nod3;
		eilerFunc /= nod3;
	}

	int d = gcd(leftMn, eilerFunc, u, v);

	int u1, v1;
	int temp = gcd(d, eilerFunc, u1, v1);
	int inv_d = (u1 % eilerFunc + eilerFunc) % eilerFunc;
	i = 0;

	int copyRightMn = rightMn;

	while (i < d)
	{
		u = (u % eilerFunc + eilerFunc) % eilerFunc;
		rightMn = ((rightMn * u) % eilerFunc + i * (eilerFunc * inv_d)) % eilerFunc;
		if (PowerUp(gFormed, rightMn, p) == a)
		{
			cout << "Ответ: " << rightMn << endl;
			return 0;
		}

		i++;
	}

	if (nod3 > 1)
	{
		u = (u % eilerFunc + eilerFunc) % eilerFunc;
		copyRightMn = ((copyRightMn * u) % eilerFunc) % eilerFunc;
		
		while (copyRightMn < p)
		{
			copyRightMn += eilerFunc;

			if (PowerUp(gFormed, copyRightMn, p) == a)
			{
				cout << "Ответ: " << copyRightMn << endl;
				break;
			}
		}
	}

	return 0;
}
