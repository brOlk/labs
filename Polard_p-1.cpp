﻿#include <iostream>
#include <cmath>
#include <list>
#include <set>

using namespace std;

int NextPrimeNumber(int n)
{
	bool flag = true;
	int check = 1;

	while (flag)
	{
		check = 1;
		n++;
		for (int i = 2; i <= n; i++)
		{
			if (n % i == 0)
			{
				check++;
			}
		}
		if (check == 2)
		{
			return n;
		}
	}
}

int NodBin(int a, int b) {
	int shift = 0;

	if (!a)
		return b;
	if (!b)
		return a;

	for (shift = 0; !((a | b) & 1); ++shift) {
		a >>= 1;
		b >>= 1;
	}

	while (!(a & 1))
	{
		a >>= 1;
	}

	do {
		while (!(b & 1))
		{
			b >>= 1;
		}

		if (a < b)
		{
			b -= a;
		}
		else
		{
			a -= (b = a - b);
		}

		b >>= 1;
	} while (b);

	return a << shift;
}

int Valid(int n)
{
	while (n % 2 == 0)
	{
		n >>= 1;
	}

	return n;
}

list<int> ToBin(int a)
{
	list<int> bin;
	int temp = 0;

	while (a)
	{
		temp = a & 1;
		bin.push_front(temp);
		a >>= 1;
	}

	return bin;
}

int PowerUp(int a, int power, int m)
{
	list<int> bin = ToBin(power);
	int z = a;
	
	auto iter = bin.begin();
	iter++;

	for (auto i = iter; i != bin.end(); i++)
	{
		z = z * z % m;
		if (*i == 1)
		{
			z = z * a % m;
		}
	}
	
	return z;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int n = 0;
	int b = 0;
	int e = 0, d = 0, q = 2;
	float a = 0;

	cout << "Введите n (нечетное, составное): ";
	cin >> n;

	while (n < 0)
	{
		cout << "Вы ввели отрицательное число введите заново: ";
		cin >> n;
	}

	n = Valid(n);
	cout << "Исправленное число: " << n << endl;

	cout << "Введите границу гладкости: ";
	cin >> b;

	a = 2 + rand() % (n - 2);
	cout << "A: " << a << endl;

	d = NodBin(a, n);
	if (d > 1)
	{
		cout << "Ответ: " << d << endl;
		return 0;
	}

	int power = 0;

	while (q <= b)
	{
		e = round(log(n) / log(q));

		power = (int)pow(q, e);
		a = PowerUp(a, power, n);

		if (a == 1)
		{
			a = 2 + rand() % (n - 2);
			cout << "A: " << a << endl;

			d = NodBin(a, n);
			if (d > 1)
			{
				cout << "Ответ: " << d << endl;
				return 0;
			}
			q = 2;
		}

		d = NodBin(a - 1, n);
		if (d > 1)
		{
			cout << "Ответ: " << d << endl;
			return 0;
		}

		q = NextPrimeNumber(q);
	}

	cout << "Фатальный отказ" << endl;
}